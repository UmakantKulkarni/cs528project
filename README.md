# cs528project



## Getting started

As a part of this project, I plan to capture the regular and VPN encrypted packet traces for VoIP applications and streaming applications. In the next step, I will extract the unidirectional flows between two endpoints based on 5-tuple. And then these flows will be converted into the histogram where x-axis will represent the time of packet arrival and y-axis will represents its size. This image can be then fed to analytics or machine learning model for training whose inferences can then be used in data-plane devices for real-time application identification.
